﻿using EasyModbus;
using Modbus.Device;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModbuSDemo
{
    public partial class Form1 : Form
    {
        TcpClient tcpClient;
        ModbusIpMaster master;
        float wartosc;
        string ip;
        int a = 0;
        public Form1()
        {
            InitializeComponent();
        }
        void pobierz(ushort pozycja)
        {
            ushort[] read = master.ReadHoldingRegisters(1, pozycja, 2);

            byte[] bytes = new byte[4];
            bytes[0] = (byte)(read[1] & 0xFF);
            bytes[1] = (byte)(read[1] >> 8);
            bytes[2] = (byte)(read[0] & 0xFF);
            bytes[3] = (byte)(read[0] >> 8);

            wartosc = BitConverter.ToSingle(bytes, 0);
        }
        private void przesun()
        {
            int x = chart1.Series[0].Points.Count - 1;

            for (int i = 1; i <= x; i++)
            {
                chart1.Series[0].Points.ElementAt(i - 1).YValues[0] = chart1.Series[0].Points.ElementAt(i).YValues[0];
                chart1.Series[1].Points.ElementAt(i - 1).YValues[0] = chart1.Series[1].Points.ElementAt(i).YValues[0];
                chart1.Series[2].Points.ElementAt(i - 1).YValues[0] = chart1.Series[2].Points.ElementAt(i).YValues[0];
            }
        }
        private void opcjeWykres()
        {
            chart1.ChartAreas[0].AxisX.Minimum = 0;
            chart1.ChartAreas[0].AxisX.Maximum = 10;
            chart1.ChartAreas[0].AxisY.Minimum = 200;
            chart1.ChartAreas[0].AxisY.Maximum = 250;
            chart1.ChartAreas[0].AxisX.Interval = 1;
        }
        private void button1_Click(object sender, EventArgs e)
        {
              ip = textBox1.Text;
             
             tcpClient = new TcpClient(ip, 502);
             master = ModbusIpMaster.CreateIp(tcpClient);
            opcjeWykres();
            timer1.Start();
            textBox1.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = true;
             
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            string formatWyjscia = "0.00";
            #region POBIERANIE
            //napiecia
            pobierz(1);
            v1txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(3);
            v2txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(5);
            v3txt.Text = wartosc.ToString(formatWyjscia);
            ////////////////////////////////

            //prady
            pobierz(13);
            i1txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(15);
            i2txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(17);
            i3txt.Text = wartosc.ToString(formatWyjscia);
            ////////////////////////////////

            //pozorna
            pobierz(19);
            mp1txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(21);
            mp2txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(23);
            mp3txt.Text = wartosc.ToString(formatWyjscia);
            /////////////////////////////////

            //czynna
            pobierz(25);
            mc1txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(27);
            mc2txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(29);
            mc3txt.Text = wartosc.ToString(formatWyjscia);
            /////////////////////////////////

            //bierna
            pobierz(31);
            mb1txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(33);
            mb2txt.Text = wartosc.ToString(formatWyjscia);
            pobierz(35);
            mb3txt.Text = wartosc.ToString(formatWyjscia);
            /////////////////////////////////
            #endregion

            #region WYKRES


            if (a >= 10)
            {
                przesun();
                chart1.Series[0].Points.ElementAt(chart1.Series[0].Points.Count - 1).YValues[0] = float.Parse(v1txt.Text);
                chart1.Series[1].Points.ElementAt(chart1.Series[0].Points.Count - 1).YValues[0] = float.Parse(v2txt.Text);
                chart1.Series[2].Points.ElementAt(chart1.Series[0].Points.Count - 1).YValues[0] = float.Parse(v3txt.Text);
            }
            else
            {
                chart1.Series[0].Points.Add(float.Parse(v1txt.Text));
                chart1.Series[1].Points.Add(float.Parse(v2txt.Text));
                chart1.Series[2].Points.Add(float.Parse(v3txt.Text));
                a++;
            }
            chart1.Invalidate();


            #endregion
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            tcpClient.GetStream().Close();
            tcpClient.Close();
            button1.Enabled = true;
            textBox1.Enabled = true;
            button2.Enabled = false;
        }
    }
}
