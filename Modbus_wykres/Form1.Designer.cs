﻿namespace ModbuSDemo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.i3txt = new System.Windows.Forms.TextBox();
            this.i2txt = new System.Windows.Forms.TextBox();
            this.i1txt = new System.Windows.Forms.TextBox();
            this.mc3txt = new System.Windows.Forms.TextBox();
            this.v3txt = new System.Windows.Forms.TextBox();
            this.mc2txt = new System.Windows.Forms.TextBox();
            this.mb3txt = new System.Windows.Forms.TextBox();
            this.mc1txt = new System.Windows.Forms.TextBox();
            this.mp3txt = new System.Windows.Forms.TextBox();
            this.mp2txt = new System.Windows.Forms.TextBox();
            this.mp1txt = new System.Windows.Forms.TextBox();
            this.mb2txt = new System.Windows.Forms.TextBox();
            this.mb1txt = new System.Windows.Forms.TextBox();
            this.v2txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.v1txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button2 = new System.Windows.Forms.Button();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(109, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(132, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Text = "192.168.1.145";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.i3txt);
            this.groupBox1.Controls.Add(this.i2txt);
            this.groupBox1.Controls.Add(this.i1txt);
            this.groupBox1.Controls.Add(this.mc3txt);
            this.groupBox1.Controls.Add(this.v3txt);
            this.groupBox1.Controls.Add(this.mc2txt);
            this.groupBox1.Controls.Add(this.mb3txt);
            this.groupBox1.Controls.Add(this.mc1txt);
            this.groupBox1.Controls.Add(this.mp3txt);
            this.groupBox1.Controls.Add(this.mp2txt);
            this.groupBox1.Controls.Add(this.mp1txt);
            this.groupBox1.Controls.Add(this.mb2txt);
            this.groupBox1.Controls.Add(this.mb1txt);
            this.groupBox1.Controls.Add(this.v2txt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.v1txt);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(60, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(346, 246);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wartości";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(133, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Faza 1";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(198, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 32;
            this.label6.Text = "Faza 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(263, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Faza 3";
            // 
            // i3txt
            // 
            this.i3txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.i3txt.Location = new System.Drawing.Point(261, 71);
            this.i3txt.Name = "i3txt";
            this.i3txt.ReadOnly = true;
            this.i3txt.Size = new System.Drawing.Size(57, 20);
            this.i3txt.TabIndex = 30;
            // 
            // i2txt
            // 
            this.i2txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.i2txt.Location = new System.Drawing.Point(198, 71);
            this.i2txt.Name = "i2txt";
            this.i2txt.ReadOnly = true;
            this.i2txt.Size = new System.Drawing.Size(57, 20);
            this.i2txt.TabIndex = 29;
            // 
            // i1txt
            // 
            this.i1txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.i1txt.Location = new System.Drawing.Point(135, 71);
            this.i1txt.Name = "i1txt";
            this.i1txt.ReadOnly = true;
            this.i1txt.Size = new System.Drawing.Size(57, 20);
            this.i1txt.TabIndex = 28;
            // 
            // mc3txt
            // 
            this.mc3txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mc3txt.Location = new System.Drawing.Point(261, 151);
            this.mc3txt.Name = "mc3txt";
            this.mc3txt.ReadOnly = true;
            this.mc3txt.Size = new System.Drawing.Size(57, 20);
            this.mc3txt.TabIndex = 27;
            // 
            // v3txt
            // 
            this.v3txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.v3txt.Location = new System.Drawing.Point(261, 45);
            this.v3txt.Name = "v3txt";
            this.v3txt.ReadOnly = true;
            this.v3txt.Size = new System.Drawing.Size(57, 20);
            this.v3txt.TabIndex = 18;
            // 
            // mc2txt
            // 
            this.mc2txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mc2txt.Location = new System.Drawing.Point(198, 151);
            this.mc2txt.Name = "mc2txt";
            this.mc2txt.ReadOnly = true;
            this.mc2txt.Size = new System.Drawing.Size(57, 20);
            this.mc2txt.TabIndex = 26;
            // 
            // mb3txt
            // 
            this.mb3txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mb3txt.Location = new System.Drawing.Point(261, 123);
            this.mb3txt.Name = "mb3txt";
            this.mb3txt.ReadOnly = true;
            this.mb3txt.Size = new System.Drawing.Size(57, 20);
            this.mb3txt.TabIndex = 21;
            // 
            // mc1txt
            // 
            this.mc1txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mc1txt.Location = new System.Drawing.Point(135, 151);
            this.mc1txt.Name = "mc1txt";
            this.mc1txt.ReadOnly = true;
            this.mc1txt.Size = new System.Drawing.Size(57, 20);
            this.mc1txt.TabIndex = 25;
            // 
            // mp3txt
            // 
            this.mp3txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mp3txt.Location = new System.Drawing.Point(261, 97);
            this.mp3txt.Name = "mp3txt";
            this.mp3txt.ReadOnly = true;
            this.mp3txt.Size = new System.Drawing.Size(57, 20);
            this.mp3txt.TabIndex = 24;
            // 
            // mp2txt
            // 
            this.mp2txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mp2txt.Location = new System.Drawing.Point(198, 97);
            this.mp2txt.Name = "mp2txt";
            this.mp2txt.ReadOnly = true;
            this.mp2txt.Size = new System.Drawing.Size(57, 20);
            this.mp2txt.TabIndex = 23;
            // 
            // mp1txt
            // 
            this.mp1txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mp1txt.Location = new System.Drawing.Point(135, 97);
            this.mp1txt.Name = "mp1txt";
            this.mp1txt.ReadOnly = true;
            this.mp1txt.Size = new System.Drawing.Size(57, 20);
            this.mp1txt.TabIndex = 22;
            // 
            // mb2txt
            // 
            this.mb2txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mb2txt.Location = new System.Drawing.Point(198, 123);
            this.mb2txt.Name = "mb2txt";
            this.mb2txt.ReadOnly = true;
            this.mb2txt.Size = new System.Drawing.Size(57, 20);
            this.mb2txt.TabIndex = 20;
            // 
            // mb1txt
            // 
            this.mb1txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mb1txt.Location = new System.Drawing.Point(135, 123);
            this.mb1txt.Name = "mb1txt";
            this.mb1txt.ReadOnly = true;
            this.mb1txt.Size = new System.Drawing.Size(57, 20);
            this.mb1txt.TabIndex = 19;
            // 
            // v2txt
            // 
            this.v2txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.v2txt.Location = new System.Drawing.Point(198, 45);
            this.v2txt.Name = "v2txt";
            this.v2txt.ReadOnly = true;
            this.v2txt.Size = new System.Drawing.Size(57, 20);
            this.v2txt.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Moc czynna [W]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Moc bierna [VAr]";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 104);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Moc pozorna [VA]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 78);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Prąd [A]";
            // 
            // v1txt
            // 
            this.v1txt.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.v1txt.Location = new System.Drawing.Point(135, 45);
            this.v1txt.Name = "v1txt";
            this.v1txt.ReadOnly = true;
            this.v1txt.Size = new System.Drawing.Size(57, 20);
            this.v1txt.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Napięcie [V]";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 30);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Start";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(336, 30);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "Stop";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // chart1
            // 
            chartArea1.AxisX.LineColor = System.Drawing.Color.Red;
            chartArea1.BackColor = System.Drawing.Color.DimGray;
            chartArea1.BorderColor = System.Drawing.Color.Transparent;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorY.IsUserEnabled = true;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.Name = "Faza 1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(454, 12);
            this.chart1.Name = "chart1";
            series1.BackSecondaryColor = System.Drawing.Color.White;
            series1.BorderWidth = 2;
            series1.ChartArea = "Faza 1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Color = System.Drawing.Color.Lime;
            series1.Legend = "Legend1";
            series1.Name = "Napięcie faza 1";
            series2.BorderWidth = 2;
            series2.ChartArea = "Faza 1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series2.Legend = "Legend1";
            series2.Name = "Napięcie faza 2";
            series3.BorderWidth = 2;
            series3.ChartArea = "Faza 1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series3.Legend = "Legend1";
            series3.Name = "Napięcie faza 3";
            this.chart1.Series.Add(series1);
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(537, 312);
            this.chart1.TabIndex = 5;
            title1.Name = "Faza 1";
            this.chart1.Titles.Add(title1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1044, 363);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Pomiar-Modbus";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox v1txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox i3txt;
        private System.Windows.Forms.TextBox i2txt;
        private System.Windows.Forms.TextBox i1txt;
        private System.Windows.Forms.TextBox mc3txt;
        private System.Windows.Forms.TextBox mc2txt;
        private System.Windows.Forms.TextBox mc1txt;
        private System.Windows.Forms.TextBox mp3txt;
        private System.Windows.Forms.TextBox mp2txt;
        private System.Windows.Forms.TextBox mp1txt;
        private System.Windows.Forms.TextBox mb3txt;
        private System.Windows.Forms.TextBox mb2txt;
        private System.Windows.Forms.TextBox mb1txt;
        private System.Windows.Forms.TextBox v3txt;
        private System.Windows.Forms.TextBox v2txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
    }
}

